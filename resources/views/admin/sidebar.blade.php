<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
          <div class="pull-left image">
                @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                    <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->name }}" class="img-circle" alt="User Image">
                @else
                    <div class="pull-left image">
                        <div class="user-avatar-nav" style="width:45px; height:45px"></div>
                    </div>                    
                @endif
          </div>
          <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                @role('admin', true)
                    <span class="pull-right label label-primary" style="margin-top:4px">
                    Admin Access
                    </span>
                @else
                    <span class="pull-right label label-warning" style="margin-top:4px">
                    User Access
                    </span>
                @endrole
          </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
            <!-- General Settings -->
            <li class="header">GENERAL SETTING</li>      
            <li class="treeview">
                <a href="#"><i class="fa fa-cogs" aria-hidden="true"></i><span>System Info</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ Request::is('companyInfo') ? 'class=active' : null }}>{!! HTML::link(url('/companyInfo'), Lang::get('titles.companyInfo')) !!}</li>                    
                    <li {{ Request::is('active-users') ? 'class=active' : null }}>{!! HTML::link(url('/active-users'), Lang::get('titles.activeUsers')) !!}</li>
                    <li {{ Request::is('logs') ? 'class=active' : null }}>{!! HTML::link(url('/logs'), Lang::get('titles.adminLogs')) !!}</li>
                    <li {{ Request::is('phpinfo') ? 'class=active' : null }}>{!! HTML::link(url('/phpinfo'), Lang::get('titles.adminPHP')) !!}</li>
                    <li {{ Request::is('routes') ? 'class=active' : null }}>{!! HTML::link(url('/routes'), Lang::get('titles.adminRoutes')) !!}</li>                                        
                </ul>
            </li>      

             <!-- General Settings -->
             <li class="header">MASTER MAINTENANCE</li>      
             <li class="treeview">
                 <a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                 <ul class="treeview-menu">
                        <li {{ Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'class=active' : null }}>{!! HTML::link(url('/users'), Lang::get('titles.adminUserList')) !!}</li>
                        <li {{ Request::is('users/create') ? 'class=active' : null }}>{!! HTML::link(url('/users/create'), Lang::get('titles.adminNewUser')) !!}</li>
                        <li {{ Request::is('users/deleted') ? 'class=active' : null }}>{!! HTML::link(url('/users/deleted'), Lang::get('titles.deleteUsers')) !!}</li>

                 </ul>
             </li>    
            <li class="treeview">
                <a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>Sections</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">                        
                    <li {{ Request::is('sections') ? 'class=active' : null }}>{!! HTML::link(url('/sections'),'Show Section List' )!!}</li>
                    <li {{ Request::is('sections/create') ? 'class=active' : null }}>{!! HTML::link(url('/sections/create'), 'New Section') !!}</li>                              
                </ul>
            </li>  
            <li class="treeview">
                <a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>Position</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">                        
                    <li {{ Request::is('positions') ? 'class=active' : null }}>{!! HTML::link(url('/positions'),'Show Position List' )!!}</li>
                    <li {{ Request::is('positions/create') ? 'class=active' : null }}>{!! HTML::link(url('/positions/create'), 'New Position') !!}</li>                              
                </ul>
            </li> 
            <li class="treeview">
                <a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>Job Requirement</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">                        
                    <li {{ Request::is('requirements') ? 'class=active' : null }}>{!! HTML::link(url('/requirements'),'Show Requirement List' )!!}</li>
                    <li {{ Request::is('requirements/create') ? 'class=active' : null }}>{!! HTML::link(url('/requirements/create'), 'New Requirement') !!}</li>                              
                </ul>
            </li>
            <!-- 
            <li class="treeview">
                    <a href="#"><i class="fa fa-users" aria-hidden="true"></i><span>Employee</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">                           
                            <li {{ Request::is('employees') ? 'class=active' : null }}>{!! HTML::link(url('/employees'), Lang::get('titles.showEmployees')) !!}</li>
                            <li {{ Request::is('employees/create') ? 'class=active' : null }}>{!! HTML::link(url('/employees/create'), Lang::get('titles.createEmployee')) !!}</li>
                    </ul>
            </li>
            -->   
            
        </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>