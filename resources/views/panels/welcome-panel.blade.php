@php

    $levelAmount = 'level';

    if (Auth::User()->level() >= 2) {
        $levelAmount = 'levels';

    }

@endphp


<div class="panel panel-primary @role('admin', true) panel-info  @endrole" style="height:650px">
    <div class="panel-heading">

        Welcome {{ Auth::user()->name }}

        @role('admin', true)
            <span class="pull-right label label-primary" style="margin-top:4px">
            Admin Access
            </span>
        @else
            <span class="pull-right label label-warning" style="margin-top:4px">
            User Access
            </span>
        @endrole

    </div>
    <div class="panel-body" style="text-align: center">       
        <p style="font-size: 85px">
            WELCOME TO            
        </p>
        <p style="font-size: 60px">
            <b style="color: blue">NEW</b> <b style="color:red">COMPUTER</b> <b style="color:green">TECHNOLOGY</b> <b>CONSULTING</b>
        </p>
        <p style="font-size: 85px">
            SYSTEM
        </p>
    </div>
</div>
