@extends('layouts.app_admin')

@section('template_title')
  View Position
@endsection

@section('template_fastload_css')
@endsection

@section('content')

        <div class="panel panel-primary">
          <div class="panel-heading">

            View Position

            <a href="/positions" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Positions</span>
            </a>

          </div>
          <div class="panel-body">
            {!! Form::model($position, array('action' => array('PositionController@update', $position->position_code  ), 'method' => 'PUT')) !!}

              <div class="form-group has-feedback row {{ $errors->has('position_code') ? ' has-error ' : '' }}">
                {!! Form::label('position_code', 'Position Code', array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('position_code', NULL, array('id' => 'position_code', 'class' => 'form-control', 'placeholder' => 'Input Position Code')) !!}
                    <label class="input-group-addon" for="position_code"><i class="fa fa-fw" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('position_code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('position_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('position_name') ? ' has-error ' : '' }}">
                  {!! Form::label('position_name', 'Position Name', array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::text('position_name', NULL, array('id' => 'position_name', 'class' => 'form-control', 'placeholder' => 'Input Position Name')) !!}
                      <label class="input-group-addon" for="position_name"><i class="fa fa-fw" aria-hidden="true"></i></label>
                    </div>
                    @if ($errors->has('position_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('position_name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                
              {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . 'Update Position', array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}

            {!! Form::close() !!}

          </div>
        </div>

@endsection

@section('footer_scripts')
@endsection
