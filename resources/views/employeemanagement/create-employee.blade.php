@extends('layouts.app_admin')

@section('template_title')
  Create New Employee
@endsection

@section('template_fastload_css')
@endsection

@section('content')

        <div class="panel panel-primary">
          <div class="panel-heading">

            Create New Employee

            <a href="/employees" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Users</span>
            </a>

          </div>
          <div class="panel-body">

            {!! Form::open(array('action' => 'EmployeeController@store')) !!}

              <div class="form-group has-feedback row {{ $errors->has('first_name') ? ' has-error ' : '' }}">
                {!! Form::label('first_name', trans('forms.create_employee_label_firstname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('first_name', NULL, array('id' => 'first_name', 'class' => 'form-control', 'placeholder' => trans('forms.create_employee_ph_firstname'))) !!}
                    <label class="input-group-addon" for="name"><i class="fa fa-fw {{ trans('forms.create_employee_icon_firstname') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('last_name') ? ' has-error ' : '' }}">
                {!! Form::label('last_name', trans('forms.create_employee_label_lastname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('last_name', NULL, array('id' => 'last_name', 'class' => 'form-control', 'placeholder' => trans('forms.create_employee_ph_lastname'))) !!}
                    <label class="input-group-addon" for="name"><i class="fa fa-fw {{ trans('forms.create_employee_icon_lastname') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('gender') ? ' has-error ' : '' }}">
                {!! Form::label('gender', trans('forms.create_employee_label_gender'), array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                      {!! Form::select('gender', array(null => 'Please select one option') + $employee->getGenders(), null, ['class'=>'form-control']) !!}
                    <label class="input-group-addon" for="name"><i class="fa fa-fw {{ trans('forms.create_employee_icon_gender') }}" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('phone_no') ? ' has-error ' : '' }}">
                  {!! Form::label('phone_no', trans('forms.create_employee_label_phone_no'), array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::text('phone_no', NULL, array('id' => 'phone_no', 'class' => 'form-control', 'placeholder' => trans('forms.create_employee_ph_phone_no'))) !!}
                      <label class="input-group-addon" for="name"><i class="fa fa-fw {{ trans('forms.create_employee_icon_phone_no') }}" aria-hidden="true"></i></label>
                    </div>
                    @if ($errors->has('phone_no'))
                      <span class="help-block">
                          <strong>{{ $errors->first('phone_no') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              
              {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . trans('forms.create_Employee_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}

            {!! Form::close() !!}

          </div>
        </div>

@endsection

@section('footer_scripts')
@endsection
