@extends('layouts.app_admin')

@section('template_title')
  Create New Job Requirement
@endsection

@section('template_fastload_css')
@endsection

@section('content')

        <div class="panel panel-primary">
          <div class="panel-heading">

            Create New Job Requirement

            <a href="/sections" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Job Requirement List</span>
            </a>

          </div>
          <div class="panel-body">
            
            {!! Form::open(array('action' => 'RequirementController@store')) !!}

            <!-- Location -->
              <div class="form-group has-feedback row {{ $errors->has('location') ? ' has-error ' : '' }}">
                {!! Form::label('location', 'Location', array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('location', NULL, array('id' => 'location', 'class' => 'form-control', 'placeholder' => 'Input Location')) !!}                   
                  </div>
                  @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

            <!-- Responsibilities -->
            <div class="form-group has-feedback row {{ $errors->has('responsiblities') ? ' has-error ' : '' }}">
              {!! Form::label('responsiblities', 'Location', array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('responsiblities', NULL, array('id' => 'responsiblities', 'class' => 'form-control', 'placeholder' => 'Input responsiblities')) !!}                   
                </div>
                @if ($errors->has('responsiblities'))
                  <span class="help-block">
                      <strong>{{ $errors->first('responsiblities') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <!-- Qualifications -->
            <div class="form-group has-feedback row {{ $errors->has('qualifications') ? ' has-error ' : '' }}">
              {!! Form::label('qualifications', 'Location', array('class' => 'col-md-3 control-label')); !!}
              <div class="col-md-9">
                <div class="input-group">
                  {!! Form::text('qualifications', NULL, array('id' => 'qualifications', 'class' => 'form-control', 'placeholder' => 'Input qualifications')) !!}                   
                </div>
                @if ($errors->has('qualifications'))
                  <span class="help-block">
                      <strong>{{ $errors->first('qualifications') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            
              {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . 'Create', array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}

            {!! Form::close() !!}

          </div>
        </div>

@endsection

@section('footer_scripts')
@endsection
