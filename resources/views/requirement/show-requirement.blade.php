@extends('layouts.app_admin')

@section('template_title')
  View Requirement
@endsection

@section('template_fastload_css')
@endsection

@section('content')

        <div class="panel panel-primary">
          <div class="panel-heading">

            View Requirement

            <a href="/requirements" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Requirement List</span>
            </a>

          </div>
          <div class="panel-body">
            {!! Form::model($requirement, array('action' => array('RequirementController@update', $requirement->location  ), 'method' => 'PUT')) !!}

              <div class="form-group has-feedback row {{ $errors->has('location') ? ' has-error ' : '' }}">
                {!! Form::label('location', 'Section Code', array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('location', NULL, array('id' => 'location', 'class' => 'form-control', 'placeholder' => 'Input Location')) !!}
                    <label class="input-group-addon" for="location"><i class="fa fa-fw" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                  @endif
                </div>
              </div>             
              
              {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . 'Update', array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}

            {!! Form::close() !!}

          </div>
        </div>

@endsection

@section('footer_scripts')
@endsection
