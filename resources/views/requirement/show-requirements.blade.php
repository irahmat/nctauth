@extends('layouts.app_admin')

@section('template_title')
  Showing Job Requirement List
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }

    </style>
@endsection

@section('content')
                <div class="panel panel-primary">
                    <div class="panel-heading">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            Showing Job Requirement List

                            <div class="btn-group pull-right btn-group-xs">

                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        Job Requirement Menu
                                    </span>
                                </button>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="/requirements/create">
                                            <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                                            Create New Job Requirement
                                        </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-condensed data-table">
                                <thead>
                                    <tr>
                                        <th>Id</th>                                        
                                        <th>Position</th>
                                        <th>Number of Person</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        <th>Opened At</th>
                                        <th>Closed At</th>
                                        <th>Created By</th>                                        
                                        <th class="hidden-sm hidden-xs hidden-md">Created at</th>
                                        <th>Updated By</th>
                                        <th class="hidden-sm hidden-xs hidden-md">Updated at</th>
                                        <th>Actions</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($requirements as $requirement)
                                        <tr>
                                            <td>{{$requirement->id}}</td>                                            
                                            <td>{{$requirement->position_code}}</td>
                                            <td>{{$requirement->no_of_person}}</td>
                                            <td>{{$requirement->location}}</td>
                                            <td>{{$requirement->status}}</td>
                                            <td>{{$requirement->opened_at}}</td>
                                            <td>{{$requirement->closed_at}}</td>
                                            <td class="hidden-sm hidden-xs hidden-md">{{$requirement->created_at}}</td>
                                            <td>{{$requirement->created_by}}</td>
                                            <td class="hidden-sm hidden-xs hidden-md">{{$requirement->updated_at}}</td>
                                            <td>{{$requirement->updated_by}}</td>
                                            <td>
                                                {!! Form::open(array('url' => 'requirements/' . $requirement->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> Job Requirement</span>', array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Employee', 'data-message' => 'Are you sure you want to delete this Job Requirement ?')) !!}
                                                {!! Form::close() !!}
                                            </td>                                            
                                            <td>
                                                <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('requirements/' . $requirement->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                    <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"> Job Requirement</span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')

    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    {{--
        @include('scripts.tooltips')
    --}}
@endsection
