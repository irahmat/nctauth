@extends('layouts.app_admin')

@section('template_title')
  Editing Company Information
@endsection

@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }
  </style>
@endsection

@section('content')

        <div class="panel panel-default">
          <div class="panel-heading">

            <strong>Editing Company Information:</strong>

            <a href="/users" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              <span class="hidden-xs">Back to </span>Users
            </a>

          </div>

          {!! Form::model($companyInfo, array('action' => array('CompanyInfoController@update', $companyInfo->company_code  ), 'method' => 'PUT')) !!}

            {!! csrf_field() !!}

            <div class="panel-body">

                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                    {!! Form::label('company_code', 'Company Code' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="col-md-9">
                    <div class="input-group">
                        {!! Form::text('company_code', old('company_code'), array('id' => 'company_code', 'class' => 'form-control', 'placeholder' => 'Company Code', 'readOnly')) !!}
                        <label class="input-group-addon" for="company_code"><i class="fa fa-fw fa-user }}" aria-hidden="true"></i></label>
                    </div>
                    </div>
                </div>

                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                    {!! Form::label('company_name', 'Company Name' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="col-md-9">
                    <div class="input-group">
                        {!! Form::text('company_name', old('company_name'), array('id' => 'company_name', 'class' => 'form-control', 'placeholder' => 'Company Name')) !!}
                        <label class="input-group-addon" for="company_name"><i class="fa fa-fw fa-user }}" aria-hidden="true"></i></label>
                    </div>
                    </div>
                </div>

                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                    {!! Form::label('company_email', 'Company Email' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="col-md-9">
                      <div class="input-group">
                        {!! Form::text('company_email', old('company_email'), array('id' => 'company_email', 'class' => 'form-control', 'placeholder' => 'Company Email Address')) !!}
                        <label class="input-group-addon" for="company_email"><i class="fa fa-fw fa-user }}" aria-hidden="true"></i></label>
                      </div>
                    </div>
                </div>

                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                    {!! Form::label('company_address_en', 'Company Address in English' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="col-md-9">
                      <div class="input-group">
                        {!! Form::text('company_address_en', old('company_address_en'), array('id' => 'company_address_en', 'class' => 'form-control', 'placeholder' => 'Company Address in English')) !!}
                        <label class="input-group-addon" for="company_address_en"><i class="fa fa-fw fa-user }}" aria-hidden="true"></i></label>
                      </div>
                    </div>
                </div>

                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                  {!! Form::label('company_address_th', 'Company Address in Thailand' , array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::text('company_address_th', old('company_address_th'), array('id' => 'company_address_th', 'class' => 'form-control', 'placeholder' => 'Company Address in Thailand')) !!}
                      <label class="input-group-addon" for="company_address_th"><i class="fa fa-fw fa-user }}" aria-hidden="true"></i></label>
                    </div>
                  </div>
              </div>

                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                    {!! Form::label('company_contact_no', 'Company Contct No' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="col-md-9">
                        <div class="input-group">
                            {!! Form::text('company_contact_no', old('company_contact_no'), array('id' => 'company_contact_no', 'class' => 'form-control', 'placeholder' => 'Company Contact Number')) !!}
                            <label class="input-group-addon" for="company_contact_no"><i class="fa fa-fw fa-user }}" aria-hidden="true"></i></label>
                       </div>
                    </div>
                </div>

            </div>

            <div class="panel-footer">

              <div class="row">
                <div class="col-xs-6">
                  {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> Save Changes', array('class' => 'btn btn-success btn-block margin-bottom-1 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('modals.edit_user__modal_text_confirm_title'), 'data-message' => trans('modals.edit_user__modal_text_confirm_message'))) !!}
                </div>
              </div>
            </div>

          {!! Form::close() !!}
  </div>

  @include('modals.modal-save')
  @include('modals.modal-delete')

@endsection

@section('footer_scripts')

  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')

@endsection