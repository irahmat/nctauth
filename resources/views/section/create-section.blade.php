@extends('layouts.app_admin')

@section('template_title')
  Create New Section
@endsection

@section('template_fastload_css')
@endsection

@section('content')

        <div class="panel panel-primary">
          <div class="panel-heading">

            Create New Section

            <a href="/sections" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Sections</span>
            </a>

          </div>
          <div class="panel-body">
            
            {!! Form::open(array('action' => 'SectionController@store')) !!}

              <div class="form-group has-feedback row {{ $errors->has('section_code') ? ' has-error ' : '' }}">
                {!! Form::label('section_code', 'Section Code', array('class' => 'col-md-3 control-label')); !!}
                <div class="col-md-9">
                  <div class="input-group">
                    {!! Form::text('section_code', NULL, array('id' => 'section_code', 'class' => 'form-control', 'placeholder' => 'Input Section Code')) !!}
                    <label class="input-group-addon" for="section_code"><i class="fa fa-fw" aria-hidden="true"></i></label>
                  </div>
                  @if ($errors->has('section_code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('section_code') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('section_name') ? ' has-error ' : '' }}">
                  {!! Form::label('section_name', 'Section Name', array('class' => 'col-md-3 control-label')); !!}
                  <div class="col-md-9">
                    <div class="input-group">
                      {!! Form::text('section_name', NULL, array('id' => 'section_name', 'class' => 'form-control', 'placeholder' => 'Input Section Name')) !!}
                      <label class="input-group-addon" for="section_name"><i class="fa fa-fw" aria-hidden="true"></i></label>
                    </div>
                    @if ($errors->has('section_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('section_name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group has-feedback row {{ $errors->has('section_fullname') ? ' has-error ' : '' }}">
                    {!! Form::label('section_fullname', 'Section Full Name', array('class' => 'col-md-3 control-label')); !!}
                    <div class="col-md-9">
                      <div class="input-group">
                        {!! Form::text('section_fullname', NULL, array('id' => 'section_fullname', 'class' => 'form-control', 'placeholder' => 'Input Section Full Name')) !!}
                        <label class="input-group-addon" for="section_fullname"><i class="fa fa-fw" aria-hidden="true"></i></label>
                      </div>
                      @if ($errors->has('section_fullname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('section_fullname') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
              
              {!! Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . 'Create Section', array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}

            {!! Form::close() !!}

          </div>
        </div>

@endsection

@section('footer_scripts')
@endsection
