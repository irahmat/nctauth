Dear Employeer,

<p>Please find attached a copy of my resume for the {{ $sender->position }}.</p>

<p>I am looking forward to meeting you in person to share my experience.</p>
 
Sincerely,
<br/>
<i>{{ $sender->fullname }}</i>
<br/>
<i>{{ $sender->contact_no }}</i>