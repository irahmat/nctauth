<?php

return [

    // Flash Messages
    'createSuccess'   => 'Successfully created Employee! ',
    'updateSuccess'   => 'Successfully updated Employee! ',
    'deleteSuccess'   => 'Successfully deleted Employee! ',
    'deleteSelfError' => 'You cannot delete yourself! ',

    // Show Employee Tab
    'viewProfile'            => 'View Profile',
    'editEmployee'           => 'Edit Employee',
    'deleteEmployee'         => 'Delete Employee',
    'EmployeesBackBtn'       => 'Back to Employees',
    'EmployeesPanelTitle'    => 'Employee Information',
    'labelFirstName'         => 'First Name:',
    'labelLastName'          => 'Last Name:',
    'labelGender'            => 'Gender:',
    'labelPhoneNo'           => 'Phone No.:',    
    'labelCreatedAt'         => 'Created At:',
    'labelUpdatedAt'         => 'Updated At:',        
    'EmployeesDeletedPanelTitle' => 'Deleted Employee Information',
    'EmployeesBackDelBtn'        => 'Back to Deleted Employees',

    'successRestore'    => 'Employee successfully restored.',
    'successDestroy'    => 'Employee record successfully destroyed.',
    'errorEmployeeNotFound' => 'Employee not found.',

    'labelEmployeeLevel'  => 'Level',
    'labelEmployeeLevels' => 'Levels',

];
