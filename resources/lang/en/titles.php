<?php

return [

    'app'           => 'NCT',
    'app2'          => 'NCT Auth 1.0',
    'home'          => 'Home',
    'login'         => 'Login',
    'logout'        => 'Logout',
    'register'      => 'Register',
    'resetPword'    => 'Reset Password',
    'toggleNav'     => 'Toggle Navigation',
    'profile'       => 'Profile',
    'editProfile'   => 'Edit Profile',
    'createProfile' => 'Create Profile',

    'activation' => 'Registration Started  | Activation Required',
    'exceeded'   => 'Activation Error',

    'editProfile'    => 'Edit Profile',
    'createProfile'  => 'Create Profile',
    'adminUserList'  => 'Users Administration',
    'adminEditUsers' => 'Edit Users',
    'adminNewUser'   => 'Create New User',

    'adminThemesList' => 'Themes',
    'adminThemesAdd'  => 'Add New Theme',

    'adminLogs'   => 'Log Files',
    'adminPHP'    => 'PHP Information',
    'adminRoutes' => 'Routing Details',

    'activeUsers' => 'Active Users',
    'deleteUsers' => 'Delete Users',

    'showEmployees' => 'Show Employees',
    'createEmployee' => 'Create Employee',

    'companyInfo' => 'Company Information',
];
