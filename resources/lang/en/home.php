<?php

return [
    'home' => 'Home',
    'aboutUs' => 'About Us',
    'services' => 'Services',
    'partners' => 'Partners',
    'gallery' => 'Gallery',
    'teams' => 'Teams',
    'career' => 'Career',
    'contactUs' => 'Contact Us',

    'consulting' => 'Consulting',
    'consulting.text' => 'We can help you to solve your problem anytime and anywhere',

    'resources' => 'Resources',
    'resources.text' => 'Our Member always working with their Passion, Experienced and Certified',

    //'aboutUs.text' => 'New Computer Technology Consulting Co., LTD (NCT) is known as an IT company who provides IT Expert Services, Consulting Services, Support Services and System Development Service. NCT is supported by certified talents and always commit to deliver the best value for customers. We always give the best solution and train our employee to keep improving based on market demands, trends as well as great methodologies', //Disabled by Orranee on 06/08/2018
    'aboutUs.text' => 'We provides advanced software technology and solution packages to customers seeking a competitive edge. The wide array of services offered includes project consultation, software development, project management and implementation for the automotive, manufacturing, transportation, service industries and retails sale.',

    'ourMission' => 'Our Mission',
    'ourMission.text' => 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',

    'ourPlan' => 'Our Plan',
    'ourPlan.text' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',

    'ourVision' => 'Our Vision',
    'ourVision.text' => 'Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.',

    'services.text' => 'we provide solution to satisfy Our value customers',

    'softwareDevelopment.text' => 'In order to meet our customer’s requirements, we are able to provide system development and services suitable for every type of business.',
    'endUserSupport.text' => 'Our staff has experience providing professional support services to customers of a variety of cultures, languages, and business fields. Our experienced staff are comfortable serving customers in English, Japanese, and Thai.',
    'outsourcing.text' => 'We provide outsourcing service by sending our engineers to work at the customer’s IT department to directly support the system development.',

    //'career.text' => 'We Are Hiring !!!', //Disabled by Orranee on 06/08/2018
    'career.text' => 'Join Us',

    'ourGallery' => 'Our Gallery',

    'address' => 'Address',

];