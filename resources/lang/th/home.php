<?php

return [
    'home' => 'หน้าหลัก',
    'aboutUs' => 'เกี่ยวกับเรา',
    'services' => 'บริการ',
    'partners' => 'ลูกค้า',
    'gallery' => 'แกลอรี่',
    'teams' => 'ทีมงาน',
    'career' => 'ร่วมงานกับเรา',
    'contactUs' => 'ติดต่อเรา',

    'consulting' => 'การให้คำปรึกษา',
    'consulting.text' => 'เราสามารถช่วยคุณแก้ไขปัญหาที่เกิดขึ้นได้ทุกที่ และทุกเวลา',

    'resources' => 'Resources',
    'resources.text' => 'สมาชิกของเรานั้น ทำงานไปด้วยความชอบ ประสบการณ์ และได้รับการรับรอง',

    'aboutUs.text' => 'บริษัท นิว คอมพิวเตอร์ เทคโนโลยี คอนซัลติ้ง จำกัด (NCT) นั้นเป็นบริษัทไอทีที่เป็นที่รู้จักเกี่ยวกับการให้บริการด้านไอทีโดยผู้เชี่ยวชาญ บริการให้คำปรึกษา บริการดูแลลูกค้า และบริการด้านการพัฒนาระบบ NCT นั้นได้รับการสนับสนุนโดยพนักงานที่มีความสามารถ ผ่านการรับรอง และมุ่งมั่นที่จะให้สิ่งที่ดีที่สุดสำหรับลูกค้า เรามอบวิธีการที่ดีที่สุดและฝึกอบรมพนักงานของเรา เพื่อให้มีการพัฒนาและปรับปรุง โดยขึ้นอยู่กับความต้องการของตลาด และมีแนวโน้มที่ไปในทางที่ดี',

    'ourMission' => 'Our Mission',
    'ourMission.text' => 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',

    'ourPlan' => 'Our Plan',
    'ourPlan.text' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',

    'ourVision' => 'Our Vision',
    'ourVision.text' => 'Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.',

    'services.text' => 'เราให้บริการโซลูชั่นเพื่อตอบสนองลูกค้าของเรา',

    'softwareDevelopment.text' => 'เพื่อตอบสนองความต้องการของลูกค้า เราให้การบริการพัฒนาระบบและบริการที่เหมาะสมกับธุรกิจทุกประเภท',
    'endUserSupport.text' => 'พนักงานของเรามีประสบการณ์ในการให้บริการสนับสนุนให้แก่ลูกค้าอย่างมืออาชีพโดยมีความหลากหลายวัฒนธรรม ภาษา และธุรกิจ โดยพนักงานที่มีประสบการณ์ของเราพร้อมให้บริการลูกค้าทั้งภาษาอังกฤษ ญี่ปุ่น และไทย',
    'outsourcing.text' => 'เราให้บริการด้านเอาท์ซอร์สโดยการส่งทีมงานของเราไปทำงานที่ฝ่าย IT ของลูกค้าเพื่อสนับสนุนการพัฒนาระบบโดยตรง',

    'career.text' => 'ร่วมงานกับเรา !',

    'ourGallery' => 'แกลอรี่',

    'address' => 'ที่ตั้ง',

];