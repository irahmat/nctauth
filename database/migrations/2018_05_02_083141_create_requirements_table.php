<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position_code',5);
            $table->foreign('position_code')->references('position_code')->on('positions')->onDelete('cascade');
            $table->string('location', 100);
            $table->text('responsibilities');
            $table->text('qualifications');
            $table->string('benefit',200);
            $table->string('range_salary',100);
            $table->string('negotiable',5);
            $table->string('employement_type',100);
            $table->tinyInteger('no_of_person');            
            $table->date('opened_at');
            $table->date('closed_at');
            $table->string('status',1);

            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirements');
    }
}
