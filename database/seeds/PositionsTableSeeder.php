<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'position_code' => '001',
                'position_name' => 'Managing Resource',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '002',
                'position_name' => 'Chief Executive Officer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '003',
                'position_name' => 'Chief Operating Officer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '004',
                'position_name' => 'Project Manager',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '005',
                'position_name' => 'Senior Consultant',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '006',
                'position_name' => 'Project Administrator',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '007',
                'position_name' => 'Secretary',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '008',
                'position_name' => 'Staff',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '009',
                'position_name' => 'Senior System Engineer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '010',
                'position_name' => 'IT Support',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '011',
                'position_name' => 'Programmer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '012',
                'position_name' => 'General service officer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '013',
                'position_name' => 'Finance and Accounting officer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '014',
                'position_name' => 'System Analyst',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '015',
                'position_name' => 'System Engineer',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'position_code' => '016',
                'position_name' => 'Executive Assistant CEO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ]
        ]);    
    }
}
