<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyInfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('company_infos')->insert([
            'company_code' => 'NCT',
            'company_name' => 'NEW COMPUTER TECHNOLOGY CONSULTING CO.,LTD.',
            'company_email' => 'nctoffical@nctthai.com',
            'company_address_en' => 'Charn Issara Tower 1, Suite No.942/42, Plaza Floor. Rama4 Road, Suriyawong, Bangrak, Bangkok, Thailand 10500',
            'company_address_th' => '942/42  อาคาร ถนนพระราม 4 ถ. พระรามที่ 4 แขวง สุริยวงศ์ เขต บางรัก กรุงเทพมหานคร 10500',
            'company_contact_no' => '123123123',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),            
        ]);

    }
}
