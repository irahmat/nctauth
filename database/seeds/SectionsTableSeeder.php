<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert([
            [
                'section_code' => '10',
                'section_name' => 'HR',
                'section_fullname' => 'Human Resources',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')        
            ],
            [
                'section_code' => '20',
                'section_name' => 'AC',
                'section_fullname' => 'Accounting',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'section_code' => '30',
                'section_name' => 'IT',
                'section_fullname' => 'Information Technology',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'section_code' => '40',
                'section_name' => 'PU',
                'section_fullname' => 'Purchasing',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'section_code' => '50',
                'section_name' => 'SA',
                'section_fullname' => 'Sales',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
