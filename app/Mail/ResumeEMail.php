<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResumeEmail extends Mailable{
    use Queueable, SerializesModels;

    public $sender;

    public function __construct($sender){
        $this->sender = $sender;
    }

    public function build(){
        return $this->from($this->sender->from)
                ->view('mails.template')
                ->subject($this->sender->subject)
                ->with(
                    [
                        'testVarOne' => '1',
                        'testVarTwo' => '2',
                    ]
                )->attach(public_path('/files').'/'.$this->sender->filename,[
                    'as' => $this->sender->fullname.'_'.$this->sender->filename,
                    'mime' => 'application/pdf',
                ]);
    }
}

?>