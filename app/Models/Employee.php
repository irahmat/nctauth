<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The Database table used by the model
     * @var String
     */
    protected $table ="employee";

    /**
     * The attributes that are not mass assignable.
     * @var array
     */
    protected $guarded = ['emp_no'];

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'phone_no',
        'hire_date',
        'create_by',
        'update_by',
    ];

    protected $dates =[
        'deleted_at'
    ];

    /**
     * Create a gender list
     */
    protected $genders = [
        'M' => 'Male',
        'F' => 'Female'
    ];

    public function getGenders(){
        return $this->genders;
    }
}
