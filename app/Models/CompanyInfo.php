<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{

    protected $primaryKey = 'company_code'; // or null

    public $incrementing = false;

    //
    public function updateCompanyInfo($data){
        $companyInfo = $this->find($data['company_code']);
        $companyInfo->company_name = $data['company_name'];
        $companyInfo->company_address_en = $data['company_address_en'];
        $companyInfo->company_address_th = $data['company_address_th'];
        $companyInfo->company_email = $data['company_email'];
        $companyInfo->company_contact_no = $data['company_contact_no'];
        $companyInfo->save();
        return 1;
    }
}
