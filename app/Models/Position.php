<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $primaryKey = 'position_code'; // or null

    public $incrementing = false;

    public function savePosition($data)
    {        
        $this->position_code = $data['position_code'];
        $this->position_name = $data['position_name'];
        $this->save();

        return 1;
    }

    public function updatePosition($data)
    {        
        $this->position_code = $data['position_code'];
        $this->position_name = $data['position_name'];   
        $this->save();

        return 1;
    }
}
