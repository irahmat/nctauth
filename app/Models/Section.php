<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'sections';

    protected $primaryKey = 'section_code'; // or null

    public $incrementing = false;

    public function saveSection($data)
    {        
        $this->section_code = $data['section_code'];
        $this->section_name = $data['section_name'];
        $this->section_fullname = $data['section_fullname'];        
        $this->save();

        return 1;
    }

    public function updateSection($data)
    {        
        $this->section_code = $data['section_code'];
        $this->section_name = $data['section_name'];
        $this->section_fullname = $data['section_fullname'];        
        $this->save();

        return 1;
    }
}
