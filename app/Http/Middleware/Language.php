<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Session;
use App;
use Config;


class Language {


    public function handle($request, Closure $next)
    {
        App::setLocale(Session::get('locale'));

        App::make('config')->set('translatable.fallback_locale', Session::get('locale'));

        return $next($request);
    }


}