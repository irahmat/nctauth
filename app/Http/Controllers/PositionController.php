<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Position;

class PositionController extends Controller
{
    /**
     * create a new controller instance
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * show the application dashboard
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $positions = Position::all();
        return view('position.show-positions', compact('positions'));
    }

    public function edit($code){
        $position = Position::find($code);
        $data = [
            'position' => $position,
        ];
        return view('position.show-position')->with($data);
    }

    public function create(){
        return view('position.create-position');
    }

    public function store(Request $request){
        $position = new Position();
        $data = $this->validate($request, [
            'position_code'=>'required',
            'position_name'=> 'required'
        ]);
       
        $position->savePosition($data);
        return redirect('/positions')->with('success', 'New Position has been created!');
    }

    public function update(Request $request, $code){
        $position = Position::find($code);
        $data = $this->validate($request, [
            'position_code'=>'required',
            'position_name'=> 'required'
        ]);
       
        $position->updatePosition($data);
        return redirect('/positions')->with('success', 'The Position has been updated!');
    }

    public function destroy($code){
        $position = Position::find($code);
        $position->delete();

        return redirect('/positions')->with('success', 'The Position has been deleted!!');
    }
}
