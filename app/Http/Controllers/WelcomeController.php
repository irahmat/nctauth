<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Career;
use App\Models\CompanyInfo;
use App\Models\User;
use App\Mail\ResumeEMail;
use Illuminate\Support\Facades\Mail;
use Config;
use Session;
use App;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companyInfo = CompanyInfo::where('company_code','NCT')->first();
        $lang = Session::get('locale');
        $data = [
            'companyInfo'=> $companyInfo,
            'lang' => $lang,
        ];

        return view('index')->with($data);
    }

    public function changeLocale(Request $request)
    {
        $this->validate($request, ['locale' => 'required|in:th,en']);

        $lang = $request->locale;

        Session::put('locale', $lang);        
        
        $companyInfo = CompanyInfo::where('company_code','NCT')->first();
        $data = [
            'companyInfo'=> $companyInfo,
            'lang' => $lang,
        ];

        return redirect()->back()->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apply(Request $request)

    {

        $rules = [
            'fullname' => 'required|max:100',
            'email' => 'required|max:100',
            'contact_no' => 'required:max:30',
            'filename' => 'required',
            'filename.*' => 'mimes:doc,pdf,docx,zip',
            'filename' => 'max:1000'
        ];

        $messages = [
            'fullname.required' =>'The Full Name is required.',
            'email.required' => 'The Email Address is required.',
            'contact_no.required' => 'The Contact Number is required.',
            'filename' => 'Your Resume is required.',
        ];

        $this->validate($request, $rules, $messages);
        
        $fullname = $request->input('fullname');
        $email = $request->input('email');
        $contact_no = $request->input("contact_no");        
        if($request->hasfile('filename'))
         {
                $file = $request->file('filename');
                $filename=$file->getClientOriginalName();
                $file->move(public_path().'/files/', $filename);  
         }

         $career= new Career();
         $career->fullname = $fullname;
         $career->email = $email;
         $career->contact_no = $contact_no;
         $career->filename=$filename;

         $companyInfo = CompanyInfo::where('id', 1)->first();        
         
        if($career->save()){
            $sender = new \stdClass();
            $sender->fullname = $fullname;
            $sender->filename = $filename;
            $sender->from = $email;
            $sender->contact_no = $contact_no;
            $sender->position = 'Java Programmer';
            $sender->subject = $fullname.' Resume for '.$sender->position;


        Mail::to($companyInfo->company_email)->send(new ResumeEMail($sender));
        }

        return back()->with('success', 'Your resume has been sent successfully.');
    }
}
