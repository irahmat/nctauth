<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Requirement;

class RequirementController extends Controller
{
    /**
     * create a new controller instance
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * show the application dashboard
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $requirements = Requirement::all();
        return view('requirement.show-requirements', compact('requirements'));
    }

    public function edit($code){
        $requirement = Requirement::find($code);
        $data = [
            'requirement' => $requirement,
        ];
        return view('requirement.show-requirement')->with($data);
    }

    public function create(){
        return view('requirement.create-requirement');
    }

    public function store(Request $request){
        
        return redirect('/positions')->with('success', 'New Position has been created!');
    }
}
