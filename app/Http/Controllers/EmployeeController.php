<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Http\Requests;

class EmployeeController extends Controller
{
    /**
     * create a new controller instance
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * show the application dashboard
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $employees = Employee::all();
        return view('employeemanagement.show-employees', compact('employees'));
    }

    /**
     * show the form for creating a new resource
     * @return \Illuminate\Http\Response
     */    
    public function create(){
        $employee = new Employee;
        return view('employeemanagement.create-employee', compact('employee'));
    }

    /**
     * Store a newly created resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $currentUser = Auth::user();

        $employee = Employee::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'phone_no' => $request->input('phone_no'),
            'hire_date' => now(),
            'create_by' => $currentUser->id,
            'update_by' => $currentUser->id
        ]);

        $employee->save();

        return redirect('employees')->with('success', trans('empmanagement.createSuccess'));
    }
}
