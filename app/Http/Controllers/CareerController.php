<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Career;

class CareerController extends Controller
{
    /*
    * Show the form for creating a new resource
    *
    * @return \Illuminate\Http\Response
    */
    public function create(){
        return view('career.create');
    }

    public function handleUpload(Request $request){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

        $rules = [
            'fullname' => 'required|max:100',
            'email' => 'required|max:100',
            'contact_no' => 'required:max:30',
            'filename' => 'required',
            'filename.*' => 'mimes:doc,pdf,docx,zip',
            'filename' => 'max:1000'
        ];

        $messages = [
            'fullname.required' =>'The Full Name is required.',
            'email.required' => 'The Email Address is required.',
            'contact_no.required' => 'The Contact Number is required.',
            'filename' => 'Your Resume is required.',
        ];

        $this->validate($request, $rules, $messages);
        
        $fullname = $request->input('fullname');
        $email = $request->input('email');
        $contact_no = $request->input("contact_no");        
        if($request->hasfile('filename'))
         {
                $file = $request->file('filename');
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/files/', $name);  
         }

         $career= new Career();
         $career->fullname = $fullname;
         $career->email = $email;
         $career->contact_no = $contact_no;
         $career->filename=$name;
         
        $career->save();

        return back()->with('success', 'Your files has been successfully added');
    }
}
