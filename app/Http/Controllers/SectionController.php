<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;

class SectionController extends Controller
{
    /**
     * create a new controller instance
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * show the application dashboard
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $sections = Section::all();
        return view('section.show-sections', compact('sections'));
    }

    public function edit($code){
        $section = Section::find($code);
        $data = [
            'section' => $section,
        ];
        return view('section.show-section')->with($data);
    }

    public function create(){
        return view('section.create-section', compact('section'));
    }

    public function store(Request $request){
        $section = new Section();
        $data = $this->validate($request, [
            'section_code'=>'required',
            'section_name'=> 'required',
            'section_fullname'=> 'required'
        ]);
       
        $section->saveSection($data);
        return redirect('/sections')->with('success', 'New Section has been created!');
    }

    public function update(Request $request, $code){
        $section = Section::find($code);
        $data = $this->validate($request, [
            'section_code'=>'required',
            'section_name'=> 'required',
            'section_fullname'=> 'required'
        ]);
       
        $section->updateSection($data);
        return redirect('/sections')->with('success', 'The Section has been updated!');
    }

    public function destroy($code){
        $section = Section::find($code);
        $section->delete();

        return redirect('/sections')->with('success', 'The Section has been deleted!!');
    }
}
