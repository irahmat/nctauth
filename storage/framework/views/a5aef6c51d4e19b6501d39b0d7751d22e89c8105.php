<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
          <div class="pull-left image">
                <?php if((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1): ?>
                    <img src="<?php echo e(Auth::user()->profile->avatar); ?>" alt="<?php echo e(Auth::user()->name); ?>" class="img-circle" alt="User Image">
                <?php else: ?>
                    <div class="pull-left image">
                        <div class="user-avatar-nav" style="width:45px; height:45px"></div>
                    </div>                    
                <?php endif; ?>
          </div>
          <div class="pull-left info">
                <p><?php echo e(Auth::user()->name); ?></p>
                <!-- Status -->
                <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?>
                    <span class="pull-right label label-primary" style="margin-top:4px">
                    Admin Access
                    </span>
                <?php else: ?>
                    <span class="pull-right label label-warning" style="margin-top:4px">
                    User Access
                    </span>
                <?php endif; ?>
          </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
            <!-- General Settings -->
            <li class="header">GENERAL SETTING</li>      
            <li class="treeview">
                <a href="#"><i class="fa fa-cogs" aria-hidden="true"></i><span>System Info</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li <?php echo e(Request::is('active-users') ? 'class=active' : null); ?>><?php echo HTML::link(url('/active-users'), Lang::get('titles.activeUsers')); ?></li>
                    <li <?php echo e(Request::is('logs') ? 'class=active' : null); ?>><?php echo HTML::link(url('/logs'), Lang::get('titles.adminLogs')); ?></li>
                    <li <?php echo e(Request::is('phpinfo') ? 'class=active' : null); ?>><?php echo HTML::link(url('/phpinfo'), Lang::get('titles.adminPHP')); ?></li>
                    <li <?php echo e(Request::is('routes') ? 'class=active' : null); ?>><?php echo HTML::link(url('/routes'), Lang::get('titles.adminRoutes')); ?></li>                    
                </ul>
            </li>      

             <!-- General Settings -->
             <li class="header">MASTER MAINTENANCE</li>      
             <li class="treeview">
                 <a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                 <ul class="treeview-menu">
                        <li <?php echo e(Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'class=active' : null); ?>><?php echo HTML::link(url('/users'), Lang::get('titles.adminUserList')); ?></li>
                        <li <?php echo e(Request::is('users/create') ? 'class=active' : null); ?>><?php echo HTML::link(url('/users/create'), Lang::get('titles.adminNewUser')); ?></li>
                        <li <?php echo e(Request::is('users/deleted') ? 'class=active' : null); ?>><?php echo HTML::link(url('/users/deleted'), Lang::get('titles.deleteUsers')); ?></li>

                 </ul>
             </li>     
             <li class="treeview">
                    <a href="#"><i class="fa fa-users" aria-hidden="true"></i><span>Employee</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">                           
                            <li <?php echo e(Request::is('employees') ? 'class=active' : null); ?>><?php echo HTML::link(url('/employees'), Lang::get('titles.showEmployees')); ?></li>
                            <li <?php echo e(Request::is('employees/create') ? 'class=active' : null); ?>><?php echo HTML::link(url('/employees/create'), Lang::get('titles.createEmployee')); ?></li>
                    </ul>
                </li>   
            
        </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>