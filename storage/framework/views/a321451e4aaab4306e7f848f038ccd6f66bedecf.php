<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
    <head>
        <meta charset="utf-8">
        <title><?php if(trim($__env->yieldContent('template_title'))): ?><?php echo $__env->yieldContent('template_title'); ?> | <?php endif; ?> <?php echo e(config('app.name', Lang::get('titles.app'))); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Indra Rahmat">        

        <!-- Google Fonts -->
        

        <!-- Bootstrap CSS File -->
        <link  href="<?php echo e(asset('lib/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="<?php echo e(asset('lib/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('lib/animate/animate.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('lib/ionicons/css/ionicons.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('lib/owlcarousel/assets/owl.carousel.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('lib/lightbox/css/lightbox.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('lib/formvalidation/css/formValidation.min.css')); ?>" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet">
    </head>
    <body>
        <!--==========================
            Header
        ============================-->
       <!-- Disabled language by Orranee on 06/08/2018-->

        <header id="header" class="">
            <div class="main-header">
                <span id="main-label"><?php echo e($companyInfo->company_name); ?><!--(Head Office)--><!-- Disabled by Orranee on 06/08/2018--></span>
            </div>
            <div class="container-fluid">
            
            <div id="logo" class="pull-left">
                <!--<h1><a href="#intro" class="scrollto">NCT</a></h1> -->
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="#intro" class=""><img src="<?php echo e(asset('img/nct/logo_nct.PNG')); ?>" alt="" title=""  /></a>
            </div>
        
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="#intro"><?php echo e(Lang::get('home.home')); ?></a></li>                                    
                    <li><a href="#about"><?php echo e(Lang::get('home.aboutUs')); ?></a></li>
                    <li><a href="#services"><?php echo e(Lang::get('home.services')); ?></a></li>                    
                    <li><a href="#partners"><?php echo e(Lang::get('home.partners')); ?></a></li>
                    <li><a href="#portfolio"><?php echo e(Lang::get('home.gallery')); ?></a></li>
                    <li><a href="#teams"><?php echo e(Lang::get('home.teams')); ?></a></li>
                    <li><a href="#career"><?php echo e(Lang::get('home.career')); ?></a></li>                    
                    <li><a href="#contact"><?php echo e(Lang::get('home.contactUs')); ?></a></li>
                    <!--<li>
                        <?php echo Form::open(['method' => 'POST', 'route' => 'changelocale', 'class' => 'form-inline navbar-select']); ?>


                        <div id="form-flag" class="form-group <?php if($errors->first('locale')): ?> has-error <?php endif; ?>">
                            <span aria-hidden="true"><i class="fa fa-flag" style="color:white; margin: 5px"></i></span>
                            <?php echo Form::select(
                                'locale',
                                ['en' => 'EN', 'th' => 'TH'],
                                \App::getLocale(),
                                [
                                    'id'       => 'locale',
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                    'onchange' => 'this.form.submit()',
                                ]
                            ); ?>

                            <small class="text-danger"><?php echo e($errors->first('locale')); ?></small>
                        </div>
                    
                        <div class="btn-group pull-right sr-only">
                            <?php echo Form::submit("Change", ['class' => 'btn btn-success']); ?>

                        </div>
                    
                        <?php echo Form::close(); ?>

                    </li>-->
                </ul>
            </nav><!-- #nav-menu-container -->
            </div>
        </header><!-- #header -->       

        <!--==========================
            Intro Section
        ============================-->
        <!-- Updated description and button scrollto by Orranee on 06/08/2018-->
        <section id="intro">
            <div class="intro-container">
                <div id="introCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            
                        <ol class="carousel-indicators">
                            <li data-target="#introCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#introCarousel" data-slide-to="1" class=""></li>
                            <li data-target="#introCarousel" data-slide-to="2" class=""></li>                            
                        </ol>
            
                    <div class="carousel-inner" role="listbox">
            
                      <!--<div class="carousel-item active" style="background-image: url('img/intro-carousel/1.jpg');">--> 
                      <div class="carousel-item active" style="background-image: url('img/intro-carousel/Cover2.png');">
                          <div class="carousel-container">
                          <div class="carousel-content">
                              <div style="padding:70px">&nbsp;</div>
                              <!--<a href="#about" class="btn-get-started scrollto">Get Started</a>-->
                              <a href="#about" class="btn-get-started scrollto"><i class="fa fa-chevron-down"></i></a>
                          </div>
                          </div>
                      </div>
              
                      <div class="carousel-item" style="background-image: url('img/intro-carousel/2.jpg');">
                          <div class="carousel-container">
                          <div class="carousel-content">
                              <!--<h2>We are open discussing and consulting</h2>-->
                              <!--<p>We are listening your problems and provide the best IT Expert for solving and supporting your issues.</p>-->
                              <h2>We always welcome to consult for finding out the solutions.</h2>
                              <p>Listening to your problems and providing the best solutions to grow the next step.</p>
                              <!--<a href="#services" class="btn-get-started scrollto">Our Services</a>-->
                              <a href="#about" class="btn-get-started scrollto"><i class="fa fa-chevron-down"></i></a>
                          </div>
                          </div>
                      </div>
              
                      <div class="carousel-item" style="background-image: url('img/intro-carousel/3.jpg');">
                          <div class="carousel-container">
                          <div class="carousel-content">
                              <!--<h2>We are looking a new member</h2>-->
                              <!--<p>We always invest our member to be an Expert and Certified to keep improving.</p>-->
                              <h2>We are looking for a new member.</h2>
                              <p>Our members are encouraged to be experts and always certified to make improvements.</p>
                              <!--<a href="#career" class="btn-get-started scrollto">Join Us</a>-->
                              <a href="#about" class="btn-get-started scrollto"><i class="fa fa-chevron-down"></i></a>
                          </div>
                          </div>
                      </div>

                    </div>
            
                    <a class="carousel-control-prev" data-target="#introCarousel" role="button" data-slide="prev" style="color: white">
                    <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
            
                    <a class="carousel-control-next" data-target="#introCarousel" role="button" data-slide="next" style="color: white">
                    <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
            
                </div>
            </div>
        </section><!-- #intro -->

        <main id="main">

                <!--==========================
                  Featured Services Section
                ============================-->
                <!-- Disabled Consulting, Resources by Orranee on 06/08/2018-->
                <!--<section id="featured-services">
                  <div class="container">
                    <div class="row">
            
                      <div class="col-lg-6 box">
                        <i class="ion-coffee"></i>
                        <h4 class="title"><a href=""><?php echo e(Lang::get('home.consulting')); ?></a></h4>
                        <p class="description"><?php echo e(Lang::get('home.consulting.text')); ?></p>
                      </div>
            
                      <div class="col-lg-6 box box-bg">
                        <i class="ion-person-add"></i>
                        <h4 class="title"><a href=""><?php echo e(Lang::get('home.resources')); ?></a></h4>
                        <p class="description"><?php echo e(Lang::get('home.resources.text')); ?></p>
                      </div>                                  
            
                    </div>
                  </div>
                </section>--><!-- #services -->
            
                <!--==========================
                  About Us Section
                ============================-->
                <section id="about">
                  <div class="container">
            
                    <header class="section-header">
                      <h3><?php echo e(Lang::get('home.aboutUs')); ?></h3>
                      <p><?php echo e(Lang::get('home.aboutUs.text')); ?></p>
                    </header>
                    <!-- Disabled Mission, Plan, Vision by Orranee on 06/08/2018-->
                    <!--<div class="row about-cols">
            
                      <div class="col-md-4 wow fadeInUp">
                        <div class="about-col">
                          <div class="img">
                            <img src="img/nct/our misson.jpeg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                          </div>
                          <h2 class="title"><a href="#"><?php echo e(Lang::get('home.ourMission')); ?></a></h2>
                          <p>
                              <?php echo e(Lang::get('home.ourMission.text')); ?>

                          </p>
                        </div>
                      </div>
            
                      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="about-col">
                          <div class="img">
                            <img src="img/nct/our plan.jpeg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-list-outline"></i></div>
                          </div>
                          <h2 class="title"><a href="#"><?php echo e(Lang::get('home.ourPlan')); ?></a></h2>
                          <p>
                              <?php echo e(Lang::get('home.ourPlan.text')); ?>

                          </p>
                        </div>
                      </div>
            
                      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="about-col">
                          <div class="img">
                            <img src="img/nct/our vision.jpeg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                          </div>
                          <h2 class="title"><a href="#"><?php echo e(Lang::get('home.ourVision')); ?></a></h2>
                          <p>
                            <?php echo e(Lang::get('home.ourVision.text')); ?>

                          </p>
                        </div>
                      </div>
            
                    </div>-->
            
                  </div>
                </section><!-- #about -->
            
                <!--==========================
                  Services Section 
                ============================-->
                <!-- Updated by Orranee on 06/08/2018-->
                <section id="services">
                  <div class="container">
            
                    <header class="section-header wow fadeInUp">
                      <h3><?php echo e(Lang::get('home.services')); ?></h3>
                      <p><?php echo e(Lang::get('home.services.text')); ?></p>
                    </header>
            
                    <div class="row text-center">
            
                      <div class="col-md-4 wow fadeInUp">
                        <div class="service-col">
                          <div class="img">
                            <!--<img src="img/SoftwareDevelopment.png" alt="" class="img-fluid" style="height:140px">-->
                            <img src="img/Service/Software.jpg" alt="" class="img-fluid" style="height:180px">
                          </div>
                          <h2 class="title"><a href="#">Software Development</a></h2>
                          <h5 style="color:orangered">______________</h5>
                          <p>
                              <?php echo e(Lang::get('home.softwareDevelopment.text')); ?>

                          </p>
                        </div>
                      </div>

                      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                          <div class="service-col">
                            <div class="img">
                              <!--<img src="img/customer_service.png" alt="" class="img-fluid" style="width:50%;height:33%">-->
                              <!-- <img src="img/callcenter.png" alt="" class="img-fluid" style="height:140px">-->
                              <img src="img/Service/Maintenance.jpg" alt="" class="img-fluid" style="height:180px">
                            </div>
                            <h2 class="title"><a href="#">Maintenance & support</a></h2><!-- Update word from Desktop & End user support -->
                            <h5 style="color:orangered">______________</h5>
                            <p>
                                <?php echo e(Lang::get('home.endUserSupport.text')); ?>

                            </p>
                          </div>
                        </div>
            
                      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="service-col">
                          <div class="img">
                            <!--<img src="img/outsource.png" alt="" class="img-fluid" style="width:50%;height:33%">-->
                            <!--<img src="img/outsourcing.png" alt="" class="img-fluid" style="height:140px">-->
                            <img src="img/Service/Outsourcing.jpg" alt="" class="img-fluid" style="height:180px">
                          </div>
                          <h2 class="title"><a href="#">Outsourcing</a></h2>
                          <h5 style="color:orangered">______________</h5>
                          <p>
                            <?php echo e(Lang::get('home.outsourcing.text')); ?>

                          </p>
                        </div>
                      </div>                                  
            
                    </div>                      
            
                    </div>
            
                  </div>
                </section><!-- #services -->
            
                <!--==========================
                  Call To career
                ============================-->
                <!-- Updated Title by Orranee on 06/08/2018-->
                <section id="career" class="wow fadeIn">
                  <div class="container text-center">
                    <h3><?php echo e(Lang::get('home.career.text')); ?></h3>     
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Java Programmer
                              </a>
                            </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                  <!--<h3>Basic Requirements:</h3>-->
                                  <h3>Qualifications :</h3>
                                  <ul>
                                    <li>Must possess at least a Diploma, Bachelor's Degree.</li>                         
                                    <li>Having experience at least 1 years in Java Programming.</li>
                                    <li>Required skills:JSON, XML.</li>
                                    <li>Good communication, hard working, can work with teamwork or individual.</li>
                                    <li>Have a good attitude and self motivated.</li>
                                    <li>Strong Logical and Analytical thinking.</li>
                                    <li>Strong knowledge in OOP.</li>
                                    <li>Experience in using subversion on development.</li>
                                    <li>Spring Technology (Kode JV-001) - Knowledgeable with Spring (Data, MVC, Security) and hibernate; Exp in Spring Web Service is a plus.</li>
                                    <li>JSF and Primeface Technology (Kode JV-002) - Exp in JSF and Primeface.</li>
                                    <li>Struts 1 Technology (Kode JV-003) - Exp in Struts and using Crystal Report.</li>
                                    <li>ZK Technology (Kode JV-004) - Exp in ZK Technology and mobile development is a plus.</li>
                                  </ul>
                                  <a class="cta-btn" data-toggle="modal" data-target="#uploadModal">Apply Now</a>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingTwo">
                              <h4 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            System Analyst
                          </a>
                        </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="panel-body">
                                  <!--<h3>Requirements:</h3>-->
                                  <h3>Qualifications :</h3>
                                  <ul>
                                  <li>Candidate must possess at least a Diploma, Bachelor's Degree, Computer Science/Information Technology, Engineering (Computer/Telecommunication), Engineering (Electrical/Electronic), Science & Technology or equivalent.</li>
                                  <li>Min 2 years work related experience.</li>
                                  <li>Have experiences in Telco Industry is a plus.</li>
                                  <li>Strong analytical and problem solving skills.</li>
                                  <li>Strong team work and communication skills.</li>
                                  <li>Ability to negotiate effectively.</li>
                                  <li>Result driven mindset and ability to develop solid working relationship with people in any levels.</li>
                                  <li>Can join immediately is a plus.</li>
                                  </ul>
                                  <a class="cta-btn" data-toggle="modal" data-target="#uploadModal">Apply Now</a>
                              </div>
                          </div>
                      </div>                      
                    </div>         
                  </div>                 
                  
                </section><!-- #career -->           
                                                              
                <!--==========================
                  Portfolio Section
                ============================-->
                <section id="portfolio"  class="section-bg" >
                  <div class="container">
            
                    <header class="section-header">
                      <h3 class="section-title"><?php echo e(Lang::get('home.ourGallery')); ?></h3>
                      <p>We are small family living together warmly. We have travel and party to spend time together.</p> <!-- Updated by Orranee on 06/08/2018 -->
                    </header>
                                  
                    <div class="row portfolio-container">
            
                      <div class="col-lg-4 col-md-6 portfolio-item wow fadeInUp">

                        <div class="portfolio-wrap">
                          <figure>
                            <img src="img/nct/japan_2015.jpg" class="img-fluid" alt="">
                            <a href="img/nct/japan_2015.jpg" data-lightbox="portfolio" data-title="japan 2015" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#intro" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                          </figure>
            
                          <div class="portfolio-info">
                            <h4><a href="#intro">2015</a></h4>
                            <p>Company Trip in Japan 2015</p>
                          </div>
                        </div>
                      </div>
            
                      <div class="col-lg-4 col-md-6 portfolio-item wow fadeInUp" data-wow-delay="0.1s">
                        <div class="portfolio-wrap">
                          <figure>
                            <img src="img/nct/taiwan_2017.jpg" class="img-fluid" alt="">
                            <a href="img/nct/taiwan_2017.jpg" class="link-preview" data-lightbox="portfolio" data-title="taiwan 2017" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                          </figure>
            
                          <div class="portfolio-info">
                            <h4><a href="#">2017</a></h4>
                            <p>Company Trip in Taiwan 2017</p>
                          </div>
                        </div>
                      </div>
            
                      <div class="col-lg-4 col-md-6 portfolio-item wow fadeInUp" data-wow-delay="0.2s">
                        <div class="portfolio-wrap">
                          <figure>
                            <img src="img/nct/ratchaburi_2018.jpg" class="img-fluid" alt="">
                            <a href="img/nct/ratchaburi_2018.jpg" class="link-preview" data-lightbox="portfolio" data-title="Outing 2018" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                          </figure>
            
                          <div class="portfolio-info">
                            <h4><a href="#">2018</a></h4>
                            <p>Company Outing in Ratchaburi</p>
                          </div>
                        </div>
                      </div>                     
                    </div>
            
                  </div>
                </section><!-- #portfolio -->
            
                <!--==========================
                  partners Section
                ============================-->
                <!-- Disabled hover description by Orranee on 06/08/2018-->
                <section id="partners" class="wow fadeInUp">
                  <div class="container">
            
                    <header class="section-header">
                      <h3>Our Partners</h3>
                    </header>
                    <div class="owl-carousel clients-carousel owl-centered">
                      <div class="agent">
                        <div class="image">
                          <img src="img/partners/asiabooks.png" alt="">
                          <!--<div class="contact-info list-unstyled mb-0">                              
                              <h5>Asia Books</h5>
                              <p>BJC is company bla bla</p>
                          </div>-->
                        </div>
                      </div>
                      <!-- Updated by Orranee on 06/08/2018 -->
                      <div class="agent">
                        <div class="image">
                          <img src="img/partners/HondaTrading1.png" alt="">
                          <!--<div class="contact-info list-unstyled mb-0">                              
                              <h5>Honda Trading</h5>
                              <p>Honda Trading is company bla bla</p>
                          </div>-->
                        </div>
                      </div>
                      <div class="agent">
                        <div class="image">
                          <img src="img/partners/bjc.png" alt="">
                          <!--<div class="contact-info list-unstyled mb-0">               
                              <h5>BJC</h5>               
                              <p>BJC is company bla bla</p>
                          </div>-->
                        </div>
                      </div>
                      <div class="agent">                       
                        <div class="image">
                            <img src="img/partners/broger.png" alt="">
                            <!--<div class="contact-info list-unstyled mb-0">               
                                <h5>Broger</h5>               
                                <p>Broger is company bla bla</p>
                            </div>-->
                        </div>
                      </div>
                      <div class="agent">                        
                        <div class="image">
                            <img src="img/partners/fujitsu.png" alt="" >
                            <!--<div class="contact-info list-unstyled mb-0">               
                                <h5>Fujitsu</h5>               
                                <p>Fujitsu is company bla bla</p>
                            </div>-->
                        </div>
                      </div>
                      <div class="agent">                        
                        <div class="image">
                            <img src="img/partners/hitachi.png" alt="" >
                            <!--<div class="contact-info list-unstyled mb-0">               
                                <h5>Hitachi</h5>               
                                <p>Hitachi is company bla bla</p>
                            </div>-->
                        </div>
                      </div>
                      <div class="agent">                            
                        <div class="image">
                            <img src="img/partners/subsiri.png" alt="">
                            <!--<div class="contact-info list-unstyled mb-0">               
                                <h5>Subsiri</h5>               
                                <p>Subsiri is company bla bla</p>
                            </div>-->
                        </div>
                      </div>
                      <div class="agent">                           
                        <div class="image">
                            <img src="img/partners/toyota.jpg" alt="">
                            <!--<div class="contact-info list-unstyled mb-0">               
                                <h5>Toyota</h5>               
                                <p>Toyota is company bla bla</p>
                            </div>-->
                        </div>
                      </div>
                    </div>
                  </div>
                </section><!-- #clients -->
            
                <!--==========================
                  Clients Section
                ============================-->
                <section id="teams" class="section-bg wow fadeInUp">
                  <div class="container">
            
                    <header class="section-header">
                      <h3>Work with Us</h3>
                    </header>
            
                    <div class="owl-carousel teams-carousel">
            
                      <div class="testimonial-item">
                        <img src="img/mui.jpg" class="testimonial-img" alt="">
                        <h3>Tiwakul Pensri (Mui)</h3>
                        <h4>CEO &amp; Managing Director</h4>
                        <p>
                          <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                          NCT is small but big of heart. we live by the concept work with passion. Our employee are our family. When hiring, we believe we can train the skills needed. However, we can't teach the heart.
                          <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                        </p>
                      </div>
            
                      <div class="testimonial-item">
                        <img src="img/hong.jpg" class="testimonial-img" alt="">
                        <h3>Nalinporn Santikanawong (Hong)</h3>
                        <h4>Executive Assistant CEO</h4>
                        <p>
                          <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                          NCT's started with passion, doing with love and making NCT be the one in customer mind.
                          <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                        </p>
                      </div>
            
                      <div class="testimonial-item">
                        <img src="img/peung.jpg" class="testimonial-img" alt="">
                        <h3>Wasana Jantamai (Peung)</h3>
                        <h4>Chief Operating Officer</h4>
                        <p>
                          <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                          Even though our company is small but full of awesome people.
                          <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                        </p>
                      </div>
            
                      <div class="testimonial-item">
                        <img src="img/takky.jpg" class="testimonial-img" alt="">
                        <h3>Pitak Thongchai (Takky)</h3>
                        <h4>Project Management Officer</h4>
                        <p>
                          <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                          Our working conditions are really great. I feel like we are all a big family.
                          <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                        </p>
                      </div>
            
                      <div class="testimonial-item">
                        <img src="img/indra.jpg" class="testimonial-img" alt="">
                        <h3>Indra Rahmat</h3>
                        <h4>Senior Programmer</h4>
                        <p>
                          <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                          NCT's have friendly people. when you joined, they will treat you like family.
                          <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                        </p>
                      </div>
            
                    </div>
            
                  </div>
                </section><!-- #teams -->
            
                <!--==========================
                  Contact Section
                ============================-->
                <section id="contact" class="section-bg wow fadeInUp">
                  <div class="container">
            
                    <div class="section-header">
                      <h3>Contact Us</h3>
                      <p><?php echo e($companyInfo->company_name); ?><!--(HEAD OFFICE)--><!-- Disabled by Orranee on 06/08/2018 --></p>
                    </div>
            
                    <div class="row contact-info">
            
                      <div class="col-md-4">
                        <div class="contact-address">
                          <i class="ion-ios-location-outline"></i>
                          <h3>Address</h3>
                          <address>
                              <?php if($lang == 'th'): ?>
                                <?php echo e($companyInfo->company_address_th); ?>                              
                              <?php else: ?>
                                <?php echo e($companyInfo->company_address_en); ?>

                              <?php endif; ?>
                          </address>
                        </div>
                      </div>
            
                      <div class="col-md-4">
                        <div class="contact-phone">
                          <i class="ion-ios-telephone-outline"></i>
                          <h3>Phone Number</h3>
                          <p><a href="tel:+"<?php echo e($companyInfo->company_contact_no); ?>><?php echo e($companyInfo->company_contact_no); ?></a></p>
                        </div>
                      </div>
            
                      <div class="col-md-4">
                        <div class="contact-email">
                          <i class="ion-ios-email-outline"></i>
                          <h3>Email</h3>
                          <p><a href="mailto:"<?php echo e($companyInfo->company_email); ?>><?php echo e($companyInfo->company_email); ?></a></p>
                        </div>
                      </div>
            
                    </div>                                
            
                  </div>
                </section><!-- #contact -->
            
              </main>

              <!-- Modal page -->
              <div id="successModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                  
                    <div class="modal-body">
                   
                    <h4><?php echo e(session('success')); ?></h4>
                   
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLabel">Upload Your Resume</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                          
                    </div>
                    <div class="modal-body">
                      <form method="post" action="<?php echo e(url('/')); ?>" enctype="multipart/form-data" id="applyResume" name="applyResume">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                          <label for="fullname" class="control-label">Full Name:</label>
                          <input type="text" class="form-control" id="fullname" name="fullname">
                        </div>
                        <div class="form-group">
                          <label for="email" class="control-label">Email Address:</label>
                          <input type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                          <label for="contact_no" class="control-label">Contact Number:</label>
                          <input type="text" class="form-control" id="contact_no" name="contact_no">
                        </div>
                        <div class="form-group">
                          <label for="resume" class="control-label">Resume in pdf:</label>
                          <input type="file" class="form-control" id="filename" name="filename">
                        </div>

                        <button type="submit" name="apply" id="apply" class="btn btn-primary" style="margin-top:10px">Apply</button>       
                        <button type="button" class="btn btn-danger" style="margin-top:10px" data-dismiss="modal">Close</button>                                              
                      </form>       
                    </div>                      
                  </div>
                </div>
              </div>          

             
              <!-- end Modal -->
            
              <!--==========================
                Footer
              ============================-->
              <!-- Updated company name by Orranee on 06/08/2018 -->    
              <footer id="footer">
                <div class="footer-top">
                  <div class="container">
                    <div class="row">
            
                      <div class="col-lg-3 col-md-6 footer-info">
                        <!--<h3>New Computer Technology Consulting Co., LTD.</h3>--> 
                        <h3>New Computer Technology Consulting Co., Ltd.</h3>                
                      </div>
            
                      <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                          <li><i class="ion-ios-arrow-right"></i> <a href="#intro">Home</a></li>
                          <li><i class="ion-ios-arrow-right"></i> <a href="#about">About us</a></li>
                          <li><i class="ion-ios-arrow-right"></i> <a href="#services">Services</a></li>
                        </ul>
                      </div>
            
                      <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <address>
                            <strong>Address: </strong>
                          <?php if($lang == 'th'): ?>
                            <?php echo e($companyInfo->company_address_th); ?>                              
                          <?php else: ?>
                            <?php echo e($companyInfo->company_address_en); ?>

                          <?php endif; ?>
                          </br>

                          <strong>Phone: </strong><?php echo e($companyInfo->company_contact_no); ?><br>
                          <strong>Email: </strong><?php echo e($companyInfo->company_email); ?><br>
                        </address>
            
                        <div class="social-links">
                          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        </div>                                
                      </div>

                      <div class="col-lg-3 col-md-6 footer-newsletter">
                          <h4>Location</h4>
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.8088644897657!2d100.53206481470215!3d13.730018890361544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f2bc2033193%3A0x245a54406c70d286!2sCharn+Issara+Tower+1!5e0!3m2!1sen!2sjo!4v1524134759601" frameborder="0" style="border:0; width: 100%" allowfullscreen></iframe>
                      </div>
                                             
                    </div>
                  </div>
                </div>

                
            
                <div class="container">
                  <div class="copyright">
                    &copy; Copyright <strong><?php echo e($companyInfo->company_name); ?></strong>. All Rights Reserved
                  </div>                  
                </div>
              </footer><!-- #footer -->


        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

        <!-- JavaScript Libraries -->
        <script src="<?php echo e(asset('lib/jquery/jquery.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/jquery/jquery-migrate.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/easing/easing.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/superfish/hoverIntent.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/superfish/superfish.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/wow/wow.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/waypoints/waypoints.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/counterup/counterup.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/owlcarousel/owl.carousel.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/isotope/isotope.pkgd.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/lightbox/js/lightbox.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/touchSwipe/jquery.touchSwipe.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/formvalidation/js/formValidation.min.js')); ?>"></script>
        <script src="<?php echo e(asset('lib/formvalidation/js/framework/bootstrap.min.js')); ?>"></script>

        <!-- Template Main Javascript File -->
        <script src="<?php echo e(asset('js/main.js')); ?>"></script>

        <?php if(session('success')): ?>
        <script type="text/javascript">
          $('#successModal').modal('show');
        </script>    
        <?php endif; ?> 
    </body>
</html>
