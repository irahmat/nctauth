<?php $__env->startSection('template_title'); ?>
  Create New Position
<?php $__env->stopSection(); ?>

<?php $__env->startSection('template_fastload_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

        <div class="panel panel-primary">
          <div class="panel-heading">

            Create New Position

            <a href="/positions" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Positions</span>
            </a>

          </div>
          <div class="panel-body">
            
            <?php echo Form::open(array('action' => 'PositionController@store')); ?>


              <div class="form-group has-feedback row <?php echo e($errors->has('position_code') ? ' has-error ' : ''); ?>">
                <?php echo Form::label('position_code', 'Position Code', array('class' => 'col-md-3 control-label'));; ?>

                <div class="col-md-9">
                  <div class="input-group">
                    <?php echo Form::text('position_code', NULL, array('id' => 'position_code', 'class' => 'form-control', 'placeholder' => 'Input Position Code')); ?>

                    <label class="input-group-addon" for="position_code"><i class="fa fa-fw" aria-hidden="true"></i></label>
                  </div>
                  <?php if($errors->has('position_code')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('position_code')); ?></strong>
                    </span>
                  <?php endif; ?>
                </div>
              </div>

              <div class="form-group has-feedback row <?php echo e($errors->has('position_name') ? ' has-error ' : ''); ?>">
                  <?php echo Form::label('position_name', 'Position Name', array('class' => 'col-md-3 control-label'));; ?>

                  <div class="col-md-9">
                    <div class="input-group">
                      <?php echo Form::text('position_name', NULL, array('id' => 'position_name', 'class' => 'form-control', 'placeholder' => 'Input Position Name')); ?>

                      <label class="input-group-addon" for="position_name"><i class="fa fa-fw" aria-hidden="true"></i></label>
                    </div>
                    <?php if($errors->has('position_name')): ?>
                      <span class="help-block">
                          <strong><?php echo e($errors->first('position_name')); ?></strong>
                      </span>
                    <?php endif; ?>
                  </div>
                </div>

              <?php echo Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . 'Create Position', array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )); ?>


            <?php echo Form::close(); ?>


          </div>
        </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app_admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>