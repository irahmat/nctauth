<!-- Main Header -->
<header class="main-header">

  <!-- Logo -->
  <a href="/home" class="logo"><b>NCT</b></a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                  <!-- Menu Toggle Button -->
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <?php if((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1): ?>
                            <img src="<?php echo e(Auth::user()->profile->avatar); ?>" alt="<?php echo e(Auth::user()->name); ?>" class="user-image" alt="User Image">
                        <?php else: ?>
                            <div class="user-avatar-nav"></div>
                        <?php endif; ?>                    
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"><?php echo e(Auth::user()->name); ?></span>
                  </a>
                  <ul class="dropdown-menu">
                      <!-- The user image in the menu -->
                      <li class="user-header" style="height:auto">
                            <?php if((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1): ?>
                                <img src="<?php echo e(Auth::user()->profile->avatar); ?>" alt="<?php echo e(Auth::user()->name); ?>" class="img-circle">
                            <?php else: ?>
                                <div class="user-avatar-nav" style="width:90px; height:90px"></div>
                            <?php endif; ?>
                          <p><?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?> - Web Developer<small>Member since Nov. 2012</small></p>
                      </li>

                      <!-- Menu Footer-->
                      <li class="user-footer">
                         
                            <div class="pull-left">
                                <?php echo HTML::link(url('/profile/'.Auth::user()->name), trans('titles.profile'), array('class'=>'btn btn-default btn-flat')); ?>

                            </div>
                         
                          <div class="pull-right">
                            <a href="<?php echo e(route('logout')); ?>" class="btn btn-danger btn-flat"
                                onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                <?php echo trans('titles.logout'); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                <?php echo e(csrf_field()); ?>

                            </form>
                          </div>
                      </li>
                  </ul>
              </li>
          </ul>
      </div>
  </nav>
</header>