<?php

    $levelAmount = 'level';

    if (Auth::User()->level() >= 2) {
        $levelAmount = 'levels';

    }

?>


<div class="panel panel-primary <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?> panel-info  <?php endif; ?>" style="height:650px">
    <div class="panel-heading">

        Welcome <?php echo e(Auth::user()->name); ?>


        <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?>
            <span class="pull-right label label-primary" style="margin-top:4px">
            Admin Access
            </span>
        <?php else: ?>
            <span class="pull-right label label-warning" style="margin-top:4px">
            User Access
            </span>
        <?php endif; ?>

    </div>
    <div class="panel-body" style="text-align: center">       
        <p style="font-size: 85px">
            WELCOME TO            
        </p>
        <p style="font-size: 60px">
            <b style="color: blue">NEW</b> <b style="color:red">COMPUTER</b> <b style="color:green">TECHNOLOGY</b> <b>CONSULTING</b>
        </p>
        <p style="font-size: 85px">
            SYSTEM
        </p>
    </div>
</div>
