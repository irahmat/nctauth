<?php $__env->startSection('template_title'); ?>
  Create New Job Requirement
<?php $__env->stopSection(); ?>

<?php $__env->startSection('template_fastload_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

        <div class="panel panel-primary">
          <div class="panel-heading">

            Create New Job Requirement

            <a href="/sections" class="btn btn-info btn-xs pull-right">
              <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
              Back <span class="hidden-xs">to</span><span class="hidden-xs"> Job Requirement List</span>
            </a>

          </div>
          <div class="panel-body">
            
            <?php echo Form::open(array('action' => 'RequirementController@store')); ?>


            <!-- Location -->
              <div class="form-group has-feedback row <?php echo e($errors->has('location') ? ' has-error ' : ''); ?>">
                <?php echo Form::label('location', 'Location', array('class' => 'col-md-3 control-label'));; ?>

                <div class="col-md-9">
                  <div class="input-group">
                    <?php echo Form::text('location', NULL, array('id' => 'location', 'class' => 'form-control', 'placeholder' => 'Input Location')); ?>                   
                  </div>
                  <?php if($errors->has('location')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('location')); ?></strong>
                    </span>
                  <?php endif; ?>
                </div>
              </div>

            <!-- Responsibilities -->
            <div class="form-group has-feedback row <?php echo e($errors->has('responsiblities') ? ' has-error ' : ''); ?>">
              <?php echo Form::label('responsiblities', 'Location', array('class' => 'col-md-3 control-label'));; ?>

              <div class="col-md-9">
                <div class="input-group">
                  <?php echo Form::text('responsiblities', NULL, array('id' => 'responsiblities', 'class' => 'form-control', 'placeholder' => 'Input responsiblities')); ?>                   
                </div>
                <?php if($errors->has('responsiblities')): ?>
                  <span class="help-block">
                      <strong><?php echo e($errors->first('responsiblities')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </div>

            <!-- Qualifications -->
            <div class="form-group has-feedback row <?php echo e($errors->has('qualifications') ? ' has-error ' : ''); ?>">
              <?php echo Form::label('qualifications', 'Location', array('class' => 'col-md-3 control-label'));; ?>

              <div class="col-md-9">
                <div class="input-group">
                  <?php echo Form::text('qualifications', NULL, array('id' => 'qualifications', 'class' => 'form-control', 'placeholder' => 'Input qualifications')); ?>                   
                </div>
                <?php if($errors->has('qualifications')): ?>
                  <span class="help-block">
                      <strong><?php echo e($errors->first('qualifications')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </div>
            
              <?php echo Form::button('<i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;' . 'Create', array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )); ?>


            <?php echo Form::close(); ?>


          </div>
        </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app_admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>