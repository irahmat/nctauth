<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/

//test admin-lte page
Route::get('admin_template', function () {
    return view('admin_template');
});

// Homepage Route
//Route::get('/', 'WelcomeController@welcome')->name('welcome');

// Homepage Route
Route::get('/', 'WelcomeController@index')->name('index');
Route::post('/', 'WelcomeController@apply');

Route::post('changelocale', ['as' => 'changelocale', 'uses' => 'WelcomeController@changeLocale']);

// Authentication Routes
Auth::routes();

// Public Routes
Route::group(['middleware' => ['web', 'activity']], function () {

    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // comment socialite register because this function is not needed.
    // Socialite Register Routes
    //Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    //Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity']], function () {

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity', 'twostep']], function () {

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'public.home',   'uses' => 'UserController@index']);

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home2', ['as' => 'public.home',   'uses' => 'UserController@index2']);

    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@show',
    ]);
});

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'currentUser', 'activity', 'twostep']], function () {

    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController', [
            'only' => [
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
    Route::put('profile/{username}/updateUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@deleteUserAccount',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses' => 'ProfilesController@userProfileAvatar',
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);

    // Route for section
    Route::delete('sections/{code}/delete', [
        'as'   => '{code}',
        'uses' => 'SectionController@destroy',
    ]);

    Route::get('sections/{code}/edit', [
        'as'   => '{code}',
        'uses' => 'SectionController@edit',
    ]);

    // Route for position
    Route::delete('positions/{code}/delete', [
        'as'   => '{code}',
        'uses' => 'PositionController@destroy',
    ]);

    Route::get('positions/{code}/edit', [
        'as'   => '{code}',
        'uses' => 'PositionController@edit',
    ]);

     // Route for requirement
     Route::delete('requirements/{code}/delete', [
        'as'   => '{code}',
        'uses' => 'RequirementController@destroy',
    ]);

    Route::get('requirement/{code}/edit', [
        'as'   => '{code}',
        'uses' => 'RequirementController@edit',
    ]);
    
});

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated', 'role:admin', 'activity', 'twostep']], function () {
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);

    Route::resource('users', 'UsersManagementController', [
        'names' => [
            'index' => 'users',
            'destroy' => 'user.destroy'
        ],
        'except' => [
            'deleted'
        ]
    ]);

    Route::resource('sections', 'SectionController', [
        'names' => [
            'index' => 'users'
        ],
        'except' => [
            'deleted'
        ]
    ]);

    Route::resource('positions', 'PositionController', [
        'names' => [
            'index' => 'positions'
        ],
        'except' => [
            'deleted'
        ]
    ]);

    Route::resource('requirements', 'RequirementController', [
        'names' => [
            'index' => 'positions'
        ],
        'except' => [
            'deleted'
        ]
    ]);


    /*
    * comment this code because it's not needed
    Route::resource('themes', 'ThemesManagementController', [
        'names' => [
            'index'   => 'themes',
            'destroy' => 'themes.destroy',
        ],
    ]);
    */

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('routes', 'AdminDetailsController@listRoutes');
    Route::get('active-users', 'AdminDetailsController@activeUsers');
    
    //edit company information
    Route::get('companyInfo', 'CompanyInfoController@index');
    
    Route::resource(
        'companyInfo',
        'CompanyInfoController', [
            'only' => [
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
});

Route::redirect('/php', '/phpinfo', 301);

/*
* @Author : Indra
* @Date : 20.02.2018
* @Desc : Create a new Route for additional features.
*/

//Example Route using version
/*
Route::group(['prefix' => 'v1'], function(){
    Route::resource('home', 'welcomeController', [
        'only' => ['index']
    ]);
});
*/
Route::resource('employees', 'EmployeeController', [
    'names' => [
        'index'   => 'employees',       
    ],
    'except' => [
        'deleted',
    ],
]);