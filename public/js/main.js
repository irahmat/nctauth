jQuery(document).ready(function( $ ) {

  // Header fixed and Back to top button
  var scrWidth = $(window).width();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
      $('#header').addClass('header-fixed');
      $('.header-scrolled').height(36);
      $('#header').addClass('header-scrolled');
      $('.main-header').height(0);
      $('#main-label').hide();
      if(scrWidth >= 1024){
        
        $('#logo img').css({height : 36+'px', bottom: 0});
      }else{
        $('#logo img').css({height : 36+'px', bottom: 0});
      }
    } else {
      $('.back-to-top').fadeOut('slow');
      $('#header').removeClass('header-fixed');
      $('.header-scrolled').height(72);
      $('#header').removeClass('header-scrolled');             
      $('.main-header').height(40);              
      $('#main-label').show();
      if(scrWidth >= 1024){
               
        $('#logo img').css({height : 40+'px', bottom: 40}); //Updated height from 80px to 4px by Orranee on 06/08/2018     
      }else{
        $('#logo img').css({height : 60+'px', bottom: 0});      
      }
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return false;
  });

  // Initiate the wowjs
  new WOW().init();

  // Initiate superfish on nav menu
  $('.nav-menu').superfish({
    animation: {opacity:'show'},
    speed: 400
  });

  // Mobile Navigation
  if( $('#nav-menu-container').length ) {
    var $mobile_nav = $('#nav-menu-container').clone().prop({ id: 'mobile-nav'});
    $mobile_nav.find('> ul').attr({ 'class' : '', 'id' : '' });
    $('body').append( $mobile_nav );
    $('body').prepend( '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>' );
    $('body').append( '<div id="mobile-body-overly"></div>' );
    $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

    $(document).on('click', '.menu-has-children i', function(e){
      $(this).next().toggleClass('menu-item-active');
      $(this).nextAll('ul').eq(0).slideToggle();
      $(this).toggleClass("fa-chevron-up fa-chevron-down");
    });

    $(document).on('click', '#mobile-nav-toggle', function(e){
      $('body').toggleClass('mobile-nav-active');
      $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('#mobile-body-overly').toggle();
    });

    $(document).click(function (e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
       if ( $('body').hasClass('mobile-nav-active') ) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
      }
    });
  } else if ( $("#mobile-nav, #mobile-nav-toggle").length ) {
    $("#mobile-nav, #mobile-nav-toggle").hide();
  }

  // Smoth scroll on page hash links
  $('a[href*="#"]:not([href="#"])').on('click', function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if( $('#header').length ) {
          top_space = $('#header').outerHeight();

          if( ! $('#header').hasClass('header-fixed') ) {
            top_space = top_space - 20;
          }
        }

        $('html, body').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ( $(this).parents('.nav-menu').length ) {
          $('.nav-menu .menu-active').removeClass('menu-active');
          $(this).closest('li').addClass('menu-active');
        }

        if ( $('body').hasClass('mobile-nav-active') ) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Porfolio filter
  $("#portfolio-flters li").click ( function() {
    $("#portfolio-flters li").removeClass('filter-active');
    $(this).addClass('filter-active');

    var selectedFilter = $(this).data("filter");
    $("#portfolio-wrapper").fadeTo(100, 0);

    $(".portfolio-item").fadeOut().css('transform', 'scale(0)');

    setTimeout(function() {
      $(selectedFilter).fadeIn(100).css('transform', 'scale(1)');
      $("#portfolio-wrapper").fadeTo(300, 1);
    }, 300);
  });

  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: false,
    dots: true,
    margin: 2,
    loop: true,
    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }
    }
  });

  // teams carousel (uses the Owl Carousel library)
  $(".teams-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    items: 1
  });

  // jQuery counterUp
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });

  $(".carousel-item").swipe({
    swipeLeft:function(event, direction, distance, duration, fingerCount) 
        {
            // do your swipe left actions in here, animations, fading, etc..
            $("#introCarousel").carousel('prev');
        },
        swipeRight:function(event, direction, distance, duration, fingerCount) 
        {
            // do your swipe right actions in here, animations, fading, etc..
            $("#introCarousel").carousel('next');
        },
        threshold:4,
        // set your swipe threshold above

        excludedElements:"button, input, select, textarea"
        // notice span isn't in the above list
      });
  //Google Maps JS
  //Set Map
  
// custom code
  $('#uploadModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-body input').val(recipient)
    $('#applyResume').data('formValidation').resetForm($('#applyResume'));
  })
  
  $('#applyResume').formValidation({
    framework: 'bootstrap',
    excluded: ':disabled',
    icon: {
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    },
    fields: {
        fullname: {
            validators: {
                notEmpty: {
                    message: 'The Full Name is required'
                },
                stringLength: {
                  message: 'The Full Name must be less than 100 characters',
                  max: function (value, validator, $field) {
                      return 100 - (value.match(/\r/g) || []).length;
                  }
                }
             }
          },
        email: {
            validators: {
                notEmpty: {
                    message: 'The Email Address is required'
                },
                emailAddress: {
                  message: 'The Email Address is not valid'
                },
                stringLength: {
                  message: 'The Email Address must be less than 100 characters',
                  max: function (value, validator, $field) {
                      return 100 - (value.match(/\r/g) || []).length;
                  }
                }
            }
        },
        contact_no: {
          validators: {
              notEmpty: {
                  message: 'The Contact Number is required'
              },
              stringLength: {
                message: 'The Contact Number must be less than 30 characters',
                max: function (value, validator, $field) {
                    return 30 - (value.match(/\r/g) || []).length;
                }
              }
          }
        },
        filename: {
          validators: {
              notEmpty: {
                  message: 'Your Resume is required'
              },
              file: {
                  extension: 'pdf',
                  type: 'application/pdf',
                  maxSize: 1048576,
                  message: 'Please choose your resume in pdf file and size 1Mb'
              }
          }
        }
      }
  }).on('err.field.fv', function(e, data) {
    // $(e.target)  --> The field element
    // data.fv      --> The FormValidation instance
    // data.field   --> The field name
    // data.element --> The field element
    data.fv.disableSubmitButtons(false);
}).on('success.field.fv', function(e, data) {
    // e, data parameters are the same as in err.field.fv event handler
    // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
    // - The submit button is clicked
    // - The form is invalid
    data.fv.disableSubmitButtons(false);
});


});
